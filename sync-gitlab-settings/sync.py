#!/usr/bin/env python3

"""
This script allows you to synchronise GitLab project settings.

It reads settings from sync.yml. This YAML file contains two keys:
- settings, which is a global mapping of GitLab project properties
- projects, which is a mapping from GitLab namespace (e.g. 'user/project') to
  settings just for that project (allowing you to override or add settings)

The settings that can be set are listed below in GROUP_ATTRIBUTES and
PROJECT_ATTRIBUTES.

By default, no changes are made, but the script prints the changes it would
make to the command line:

    ./sync.py

To actualy make the changes, set DRY_RUN to false:

    DRY_RUN=false ./sync.py

This requires a GITLAB_TOKEN to be set in .env (or as an environment variable).
"""

from dotenv import dotenv_values
import gitlab
import os
import re
import sys
import yaml

CYAN = '\033[0;36m'
RED = '\033[0;31m'
YELLOW = '\033[0;33m'
RESET = '\033[0m'

GROUP_ATTRIBUTES = [
    'labels',
]

PROJECT_ATTRIBUTES = [
    # General
    'name',
    'labels',
    'badges',

    # Features & visibility
    'issues_enabled',
    'lfs_enabled',
    'merge_requests_enabled',
    'packages_enabled',
    'service_desk_enabled',
    'requirements_enabled',
    'analytics_access_level',
    'container_registry_access_level',
    'environments_access_level',
    'feature_flags_access_level',
    'infrastructure_access_level',
    'monitor_access_level',
    'pages_access_level',
    'releases_access_level',
    'requirements_access_level',
    'security_and_compliance_access_level',
    'snippets_access_level',
    'wiki_access_level',

    # Access
    'request_access_enabled',

    # CI/CD
    'jobs_enabled',
    'public_jobs',
    'shared_runners_enabled',
    'auto_devops_enabled',
    'build_timeout',

    'pipeline_schedules',

    # Issues
    'autoclose_referenced_issues',
    'issues_template',

    # Merge requests
    'merge_method',
    'squash_option',
    'merge_pipelines_enabled',
    'merge_trains_enabled',
    'only_allow_merge_if_pipeline_succeeds',
    'allow_merge_on_skipped_pipeline',
    'only_allow_merge_if_all_discussions_are_resolved',
    'remove_source_branch_after_merge',
    'printing_merge_request_link_enabled',
    'suggestion_commit_message',
    'merge_requests_template',
]

def check_config(config):
    fail = False

    for attr in config['settings'].keys():
        if attr not in PROJECT_ATTRIBUTES:
            print(f'Unknown attribute {attr}')
            fail = True

    if 'groups' in config:
        for group_settings in config['groups'].values():
            for attr in group_settings.keys():
                if attr not in GROUP_ATTRIBUTES:
                    print(f'Unknown attribute {attr}')
                    fail = True

    if 'projects' in config:
        for project_settings in config['projects'].values():
            for attr in project_settings.keys():
                if attr not in PROJECT_ATTRIBUTES:
                    print(f'Unknown attribute {attr}')
                    fail = True

    if fail:
        sys.exit(1)

requirements_enabled_warning_shown = False

# obj can be a project or a group
def check_attribute(obj, config, attr, dry_run=True):
    global requirements_enabled_warning_shown

    if attr == 'requirements_enabled':
        if not requirements_enabled_warning_shown:
            print(f'\t{CYAN}ignoring requirements_enabled; cannot be set with the API yet{RESET}')
            requirements_enabled_warning_shown = True

        return False

    value = None
    if type(obj) == gitlab.v4.objects.projects.Project:
        if 'settings' in config and attr in config['settings']:
            value = config['settings'][attr]
        if obj.path_with_namespace in config['projects'] and \
                attr in config['projects'][obj.path_with_namespace]:
            value = config['projects'][obj.path_with_namespace][attr]
    elif type(obj) == gitlab.v4.objects.groups.Group:
        if 'group_settings' in config and attr in config['group_settings']:
            value = config['group_settings'][attr]
        if obj.full_path in config['groups'] and \
                attr in config['groups'][obj.full_path]:
            value = config['groups'][obj.full_path][attr]
    else:
        raise ValueError('obj must be a group or project')

    if value is None:
        return False

    if attr == 'pipeline_schedules':
        if not obj.jobs_enabled:
            return False

        current_schedules = []
        for s in obj.pipelineschedules.list(all=True):
            if s.active:
                current_schedules.append((s.ref, s.description, s.cron))

        wanted_schedules = [(s['ref'], s['description'], s['cron']) for s in value]

        if set(wanted_schedules) == set(current_schedules):
            return False

        print(f'\t{YELLOW}pipeline_schedules := {value}{RESET}')

        if dry_run:
            return False

        for s in obj.pipelineschedules.list(all=True):
            s.delete()

        for schedule in value:
            obj.pipelineschedules.create(schedule)

        return False # no update of the project needed

    if attr == 'labels':
        current_labels = []
        for l in obj.labels.list(all=True):
            if type(l) == gitlab.v4.objects.ProjectLabel and not l.is_project_label:
                # Ignore labels inherited from the group
                continue
            current_labels.append((l.name, l.color, l.description if l.description != '' else None))

        wanted_labels = [(l['name'], l['color'], l['description'] if 'description' in l else None) for l in value]

        if set(wanted_labels) == set(current_labels):
            return False

        print(f'\t{YELLOW}labels := {value}{RESET}')
        print(f'\t{RED}warning: to *rename* a label use the user interface to preserve relations to issues and MRs{RESET}')

        if dry_run:
            return False

        wanted_labels = {l['name']: [l['color'], l['description'] if 'description' in l else None] for l in value}
        for l in obj.labels.list(all=True):
            if l.name in wanted_labels:
                new = wanted_labels[l.name]
                if l.color != new[0] or l.description != new[1]:
                    print(f'\t\t{YELLOW}updating {l.name}{RESET}')
                    l.color = new[0]
                    l.description = new[1]
                    l.save()
                del wanted_labels[l.name]
            else:
                print(f'\t\t{YELLOW}deleting {l.name}{RESET}')
                l.delete()

        for name, label in wanted_labels.items():
            print(f'\t\t{YELLOW}creating {name}{RESET}')
            obj.labels.create({'name': name, 'color': label[0], 'description': label[1]})

        return False # no update of the project needed

    if attr == 'badges':
        current_badges = []
        for b in obj.badges.list(all=True):
            if type(b) == gitlab.v4.objects.ProjectBadge and b.kind != 'project':
                # Ignore badges inherited from the group
                continue
            current_badges.append((b.name, b.link_url, b.image_url))

        wanted_badges = [(b['name'], b['link_url'], b['image_url']) for b in value]

        if set(wanted_badges) == set(current_badges):
            return False

        print(f'\t{YELLOW}badges := {value}{RESET}')

        if dry_run:
            return False

        # Delete existing badges
        for b in obj.badges.list(all=True):
            if type(b) == gitlab.v4.objects.ProjectBadge and b.kind != 'project':
                # Ignore badges inherited from the group
                continue
            b.delete()

        # Created wanted badges
        for b in value:
            obj.badges.create(b)

        return False # no update of the project needed

    try:
        if getattr(obj, attr) == value:
            return False
    except:
        pass

    printable_value = value
    if type(value) == str:
        printable_value = re.sub(r'(?s)\s*\n.*', '  (... multiline value)', value)
    print(f'\t{YELLOW}{attr} := {printable_value}{RESET}')

    if dry_run:
        return False

    setattr(obj, attr, value)
    return True

if __name__ == '__main__':
    env = {**dotenv_values('.env'), **os.environ}

    only = None if len(sys.argv) < 2 else sys.argv[1:]

    if 'DRY_RUN' not in env:
        env['DRY_RUN'] = True
    else:
        env['DRY_RUN'] = bool(yaml.safe_load(env['DRY_RUN']))

    config = {}

    with open('sync.yml') as f:
        config = yaml.safe_load(f)

    check_config(config)

    gl = gitlab.Gitlab(
            'https://gitlab.com',
            private_token=env['GITLAB_TOKEN'])

    if 'groups' in config:
        for namespace, settings in config['groups'].items():
            if only is not None and namespace not in only:
                continue

            group = gl.groups.get(namespace)

            print(f'Checking {namespace}...')

            has_update = False
            for attr in GROUP_ATTRIBUTES:
                if check_attribute(group, config, attr, dry_run=env['DRY_RUN']):
                    has_update = True

            if has_update:
                project.save()

    if 'projects' in config:
        for namespace, settings in config['projects'].items():
            if only is not None and namespace not in only:
                continue

            project = gl.projects.get(namespace)

            print(f'Checking {namespace}...')

            has_update = False
            for attr in PROJECT_ATTRIBUTES:
                if check_attribute(project, config, attr, dry_run=env['DRY_RUN']):
                    has_update = True

            if has_update:
                project.save()
